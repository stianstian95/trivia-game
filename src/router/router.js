import VueRouter from 'vue-router';
import Vue from 'vue';

Vue.use(VueRouter);

/* Routes uses lazy loading */
const routes = [
  {
    path: '/',
    name: 'StartScreen',
    component: () =>
      import(
        /* webpackChunkName: "StartScreen"*/ '../components/Home/StartScreen.vue'
      )
  },
  {
    path: '/QuestionScreen',
    name: 'QuestionScreen',
    component: () =>
      import(
        /* webpackChunkName: "QuestionScreen"*/ '../components/Questions/QuestionScreen.vue'
      )
  },
  {
    path: '/ResultScreen',
    name: 'ResultScreen',
    component: () =>
      import(
        /* webpackChunkName: "ResultScreen"*/ '../components/Result/ResultScreen.vue'
      )
  }
];

/* Creats and mounts the root instance */
const router = new VueRouter({
  mode: 'history',
  routes
});

export default router;
