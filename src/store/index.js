import Vue from 'vue';
import Vuex from 'vuex';
import { questionApi } from '../api/questionApi';

Vue.use(Vuex);

/* Defines vuex states */
export default new Vuex.Store({
  state: {
    user: '',
    questions: [],
    questionIndex: 0,
    numberOfQuestions: '1',
    difficulty: 'easy',
    categories: [''],
    selectedCategory: '9',
    error: '',
    token: '',
    isLoading: true
  },

  mutations: {
    setUser: (state, payload) => {
      state.user = payload;
    },
    setQuestions: (state, payload) => {
      state.questions = payload;
    },
    setQuestionIndex: (state, payload) => {
      state.questionIndex = payload;
    },
    setAnswer: (state, payload) => {
      state.questions[state.questionIndex].answer = payload;
    },
    setNumberOfQuestions: (state, payload) => {
      state.numberOfQuestions = payload;
    },
    setDifficulty: (state, payload) => {
      state.difficulty = payload;
    },
    setCategories: (state, payload) => {
      state.categories = payload;
    },
    setSelectedCategory: (state, payload) => {
      state.selectedCategory = payload;
    },
    setError: (state, payload) => {
      state.error = payload;
    },
    setToken: (state, payload) => {
      state.token = payload;
    },
    setIsLoading: (state, payload) => {
      state.isLoading = payload;
    }
  },

  getters: {
    /* Randomizes the order of answers so that the correct answer won't be in the same spot each time */
    quizAnswers: (state) => {
      const quizAnswers = [
        ...state.questions[state.questionIndex].incorrect_answers,
        state.questions[state.questionIndex].correct_answer
      ];
      const randomOrder = (answer) => {
        for (let i = answer.length - 1; i > 0; i--) {
          const j = Math.floor(Math.random() * (i + 1));
          [answer[i], answer[j]] = [answer[j], answer[i]];
        }
        return answer;
      };
      return randomOrder(quizAnswers);
    },

    /* Sets the maxScore for a question to be 10 */
    maxScore: (state) => {
      return state.questions.length * 10;
    }
  },

  actions: {
    /**
     * Uses the Trivia API session tokens to make sure the same question does not appear twice in one quiz session
     * If the response_code is 0 (results were returned successfully) the token value is set to the token state
     */
    async getQuizToken({ commit }) {
      try {
        const [error, response] = await questionApi.getToken();

        if (response.response_code === 0) {
          commit('setToken', response.token);
        } else {
          commit('setError', `Could not retrieve token ${error}`);
        }
      } catch (error) {
        commit('setError', error.message);
      }
    },
    
    /**
     * Sets the questionsIndex to 0, isLoading to true while the quiz loads, and error to ''
     * Checks if there is a token and if not calls the getQuizToken method
     *
     * urlAlter sets values to be used in URLSearchParams as a parameter when fetching questions from the Trivia API
     * Sets questions to be the questions from the response
     *
     * After the questions has been fetched isLoading is set to false
     */
    async getQuizQuestions({ state, commit }) {
      commit('setQuestionIndex', 0);
      commit('setError', '');
      commit('setIsLoading', true);

      if (state.token === '') {
        await this.dispatch('getQuizToken');
      }

      try {
        const urlAlter = {
          amount: state.numberOfQuestions,
          difficulty: state.difficulty,
          category: state.selectedCategory,
          token: state.token
        };

        const quizInfo = new URLSearchParams(urlAlter);
        const response = await questionApi.getQuestions(quizInfo);
        commit('setQuestions', response[1].results);
      } catch (error) {
        commit('setError', error.message);
        commit('setIsLoading', false);
      }
      commit('setIsLoading', false);
    },

    /* Increases the questionIndex by 1 */
    nextQuestion({ state, commit }) {
      commit('setQuestionIndex', state.questionIndex + 1);
    },

    /**
     * isLoading is set to true while the quiz loads
     * Sets the states of error and categories to be from the response
     * in the getCategories method in questionApi.js
     *
     * After the quiz is loaded, isLoading is set to false
     */
    async getQuizCategories({ commit }) {
      commit('setIsLoading', true);

      try {
        const [error, categories] = await questionApi.getCategories();
        commit('setCategories', categories);
        commit('setError', error);
      } catch (error) {
        commit('setError', error.message);
      }
      commit('setIsLoading', false);
    }
  }
});
